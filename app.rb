# frozen_string_literal: true

require 'yaml'
require 'terminal-table'
require 'byebug'

require_relative 'lib/base'
require_relative 'lib/product'
require_relative 'lib/optional'
require_relative 'lib/promotion'
require_relative 'lib/item'
require_relative 'lib/checkout'
require_relative 'lib/store'

@store = Store.new
@checkout = Checkout.new

def prompt
  print '> '
end

def breakline
  puts "\n"
end

option = 0
while option != 7
  puts """
    Welcome to Ruby Store
    1. Products
    2. Find product
    3. Promotions
    4. Add product
    5. Add tip
    6. Total
    7. Exit
  """
  prompt

  option = gets.chomp.to_i
  breakline

  case option
  when 1
    puts @store.products
  when 2
    puts 'Please enter product code'
    prompt
    code = gets.chomp
    product = @store.find_product(code)
    breakline
    puts product.nil? ? 'Sorry, this product does not exist' : product
  when 3
    puts @store.promotions
  when 4
    puts 'Please enter code:'
    prompt
    code = gets.chomp
    puts 'Please enter quantity:'
    prompt
    quantity = gets.chomp

    breakline
    puts @checkout.add_product(code, quantity) ? 'The product was added successfully' : 'This product does not exist'
  when 5
    puts 'Please add the percentage of tip(default 10 %):'
    prompt
    value = gets.chomp
    @checkout.add_tip(value.to_f / 100)
    breakline
    puts 'Tip was added  successfully'
  when 6
    puts @checkout.total
  when 7
    puts 'Bye bye user!'
  else
    puts 'Please select a correct option'
  end
end
