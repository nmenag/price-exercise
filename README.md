# Solution to  Ruby Store Code Challenge

```yml
# en base a los siguientes datos, implementar
# un modelo que permita calcular el valor total
# para lo siguiente:

# --> 10 unidades de naranjas

# luego recalcular el total agregando lo siguiente:

# --> 15 unidades de manzanas

# y finalmente agregar al carro una bolsa y una propina,
# en este último caso donde el monto lo define el usuario.

# importante a considerar es que las promociones no
# son acumulables, es decir, solo puede haber una
# promocion activa en el carro. en caso de dos promociones
# sean aplicables, debe "ganar" la que aplique el mayor
# descuento.

```

## Class Diagram

This is the class diagram that I chose to solve the exercise. With this architecture it is possible to connect to a database and extend the store with more products or rules in a quick and easy way.

[View class Diagram](https://viewer.diagrams.net/?title=Untitled%20Diagram.drawio#R7Zxtc6o4FMc%2FjTO7L%2Bzw5ENfVvt4t7t9sLftvuqkECW3QLghVu2n3wSDgokKVmjHZaYzJYdA4PxOkvMPaRtm359eEBC6f2MHeg1Dc6YN87RhGLpmdtkvbpnNLa22OTeMCHJEpaVhgD5gcqWwjpEDo0xFirFHUZg12jgIoE0zNkAInmSrDbGXbTUEIygZBjbwZOsTcqg7t3aNztJ%2BCdHITVrW28fzMz5IKos3iVzg4EnKZJ41zD7BmM6P%2FGkfetx5iV%2BermZP3vVb%2B%2BLHXfQb%2FOz99fDPY3N%2Bs%2FMilyxegcCA7nzrj7fh%2BeWj9es5vD2%2Fm2gXJ68%2Fmk1duOEdeGPhsL4L7Tc8puKl6SzxZDRBvgcCVuoNcUAH4ozGysBDo4Ad2%2BwBIWGGd0goYhBOxAmKQ2a1XeQ512DG726eRhTYb0mp52KCPthtgcdO6czAThMq4sloZ2oM%2BJWiaQIjVuc28Y2%2BMF2DiIo6NvY8EEboNX5gXsUHZISCHqYU%2B8mN8DhwoCNKC9hxgRL8tggffn1OIoIc9wacpuJRELqA2IeUzFgVcbZliWBLepsoTpahq7eFzU2HbccSXUZ0l9Hi1ovW7ln3AsGI%2BWDZXCvbnG7lbI8ByTQHPMY9ABT2uBejdCCyg9SbLk1xeBYJ1a4Uqg84DpcTZr1igTdiobcatMzvNBWgHhzSteEZhcBGweg6rnNqLS334sW5CbNrh14cGi5yHBjEoUMBBa%2BL7hBiFNDYM60e%2B2H%2B62tHrUaLPVCflfVlmf3w6oT2ccCiDKA4nCAL3Qnk4asItM39eHukzbIEi5JOB1YGcWGexxLPKwr9aM6TDf2AP22Ts2XmgwO7Ybhxqe%2BJw9Lwt4x8%2BJPp%2FzP43evRzdnNwI0Gw9ng%2FN651O9%2BNq2WhF9i7KF4thH%2B0JUj8ZYA8BlKfruE%2BAMPiNOmLkWFKUeFqYgAD7xC7xZHiCLM70%2FmdVciI1%2BvlmjlJr0W63HO8bs0qm2JKnCcF5bt%2FfHnAY%2FSRTpzGdS7OTuzURZ2eW4GYejNXhwU2cw3tKZfHn3dzNnpSxvLTVNiCh2mwkSRuc7FIxwA72xpXfHass41jodoPkL%2FgpTOhAQAY4qzcwFzGpk98%2BuPjjudxPAvH961I61rJZbTqWhjXpqlS7eQIOYDrllSkPjDr0W0KdGP8JjYcBNK4SmmbUZwI%2FNjNXMCPUDRe%2Fbx9p%2BbmcrcTKJcS8LyJWHHyGo0o6Po7h2VJuzupAnNFQlqdnO29xWa0JLiNCTYGfN1JK4ibkXh0GacvBOHmTvYNqjCPKz3NpUYEtC7MQgoorP%2Fg87fKTswNxJVCb1KicqKQIJX67yiVDuKjF9FtayMT142fuSHcZqP6i5aDGY376BblnyT1VtGt9VACwLVtZzds7xpVF5cZbmQj%2BPRrOaYl6NpfTFHS5doVaGsp4g%2Bp465puYw5qWlnuaFjJx%2BThdSV22Q4Y39KW51TxBdMa241w%2BCFQhuNWer5vy5lZXcCytWRZyVrcvJ8PphuV5cKX9xxdR3XOxYfCkvtriyWKvd1N43%2BeAu5%2Fg2dvi7DChhE%2F6BphFbc4Z27hj7gjUV5RPL6X0A%2FBTIvtE40Q4O514%2FsBeG%2FuXLLobcfRNRx6znHgaHtyxa9oe49kbkeddkykMud%2FR7%2BHuMCPQhd7GhXYLIranvlbquVagRlc%2BnqXJK5WePNRmlUWeU5WWUumpLjjKj7OyUURorGeV33sIpf9wJCbLhiy92nR9oYrnXTETLHZDfJf2U9xLE1OsspBTcVSae6gUWCfeKbGQN80EyccEBkS%2BLaZWZpfKJ5b26WQVZMy3MtNK8USkXFH9QQREkXCeciN33B6kXdlcC2maiqn2YlRI15e1QNyFfW2Ypea0FtmmBLRGxXh6s37qXTc0VmfnizyQzSsA8JCWgjlR5RqmlQLG1ig0jQBEtoIzA8kaorR8VDj2TKIGqIuWvmOq2Lww11eJUFUl%2FxVTlPURCuNcI8yFU5fjVMrQ6Eq1K%2FzKn8a32Apl59wJZa9b8P7tJhBWX%2Fy9inkst%2F%2BuGefYf)

 The exercise is composed of the following classes:

 ### Product

  This class contains the product information, it has  attributes: price_model, code, name, price, tiers.


### Promotion

This class apply all different rules of the discount define for the store.

### Optional

This class apply all different options  available for the checkout, actually is available Tips and bags.


 ### Item

This class is associated with one checkout process. Each object from this class saves the product, the quantity, the promotion for apply the discount for each product.


### Checkout

Checkout class is where the magic happens, Each checkout object has a set of items as an array of products. With this special configuration we can control the price for each item, applying discounts, tips and the bag.

### Store

This class is used for interactive with our app



## Development

You can also run ./bin/console for an interactive prompt that will allow you to experiment.

For run all tests:

`rake test`


## Installation

this repo includes some gems for its implementation  and development

execute:

`bundle install`

## Interaction

In addition I built a Command Line Interface:

 run: `ruby app.rb`

 You can find the next options at the CLI:

 1. **Products:** Get all the products from the store.
2. **Find product:** Find a product given a specific code.
3. **Promotions:** Get all the promotions from the store.
4. **Add product:** Add a new item to checkout process.
5. **Add Tip:** Add the tip to checkout process.
6. **Total:** Get the total value for checkout process after applying all the rules.
7. **Exit:** Finish the program.


## TODO

- Improve dependencies  inject
- Improve Validations
- Add Automated code Review
- Add simplecov code coverage
