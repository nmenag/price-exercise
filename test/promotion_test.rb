# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/base'
require_relative '../lib/promotion'

class PromotionTest < Test::Unit::TestCase
  def setup
    @promotion = Promotion.new('test promotion', discount: 0.2, requirements: { min_units: 20 })
  end

  test 'initialize' do
    assert_equal @promotion.name, 'test promotion'
    assert_equal @promotion.code, 'test_promotion'
    assert_equal @promotion.discount, 0.2
    assert_equal @promotion.requirements, { min_units: 20 }
  end

  test '.find' do
    promotion = Promotion.find('one')
    assert_equal promotion.class, Promotion
    assert_equal promotion.code, 'one'
    assert_equal promotion.discount, 0.2
  end
end
