# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/base'
require_relative '../lib/product'

class ProductTest < Test::Unit::TestCase
  def setup
    @product = Product.new('test product', price_model: 'fixed', price: 12)
    @tiers = [{ from: 1, to: 10, price: 10 },
              { from: 11, to: 20, price: 9 },
              { from: 21, to: 30, price: 8 }]

    @product_with_tiers = Product.new('test product tier', price_model: 'tiered',
                                                           tiers: @tiers)
  end

  test 'initialize' do
    assert_equal @product.name, 'test product'
    assert_equal @product.code, 'test_product'
    assert_equal @product.price_model, 'fixed'
    assert_equal @product.price, 12

    assert_equal @product_with_tiers.name, 'test product tier'
    assert_equal @product_with_tiers.code, 'test_product_tier'
    assert_equal @product_with_tiers.price_model, 'tiered'
    assert_equal @product_with_tiers.price, 0.0
    assert_equal @product_with_tiers.tiers, @tiers
  end

  test '.find' do
    assert_equal Product.find('orange').class, Product
  end

  test '#tiers_value_or_price' do
    assert_equal @product.tiers_value_or_price(10), 120
    assert_equal @product.tiers_value_or_price(15), 180
    assert_equal @product_with_tiers.tiers_value_or_price(15), 135
    assert_equal @product_with_tiers.tiers_value_or_price(25), 200
  end

  test '#fixed?' do
    assert @product.fixed?
    refute @product_with_tiers.fixed?
  end

  test '#tiered?' do
    refute @product.tiered?
    assert @product_with_tiers.tiered?
  end
end
