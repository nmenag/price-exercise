# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/base'
require_relative '../lib/item'

class ItemTest < Test::Unit::TestCase
  def setup
    @product = Product.new('orange', price_model: 'fixed', price: 12)
    @tiers = [{ from: 1, to: 10, price: 10 },
              { from: 11, to: 20, price: 9 },
              { from: 21, to: 30, price: 8 }]

    @product_with_tiers = Product.new('apple', price_model: 'tiered',
                                               tiers: @tiers)

    @item = Item.new(@product.code, 10)
    @item_with_tiers = Item.new(@product_with_tiers.code, 15)
  end

  test 'initialize' do
    assert_equal @item.product.code, @product.code
    assert_equal @item.quantity, 10
  end

  test 'initialize without quantity' do
    item = Item.new(@product.code)
    assert_equal item.product.code, @product.code
    assert_equal item.quantity, 1

    item = Item.new(@product.code, 0)
    assert_equal item.product.code, @product.code
    assert_equal item.quantity, 1

    item = Item.new(@product.code, 'one')
    assert_equal item.product.code, @product.code
    assert_equal item.quantity, 1
  end

  test '#value' do
    assert_equal @item.value, 120
  end

  test '#product' do
    assert_equal @item.product.class, Product
  end

  test '#promotion' do
    assert_equal @item.promotion.discount, 0.1
    assert_equal @item.promotion.class, Promotion

    @item.quantity = 5
    assert_equal @item.promotion, nil

    @item.quantity = 22
    assert_equal @item.promotion.discount, 0.2
  end

  test '#discount' do
    assert_equal @item.discount, 12

    @item.quantity = 5
    assert_equal @item.discount, 0

    @item.quantity = 22
    assert_equal @item.discount, 52

    assert_equal @item_with_tiers.discount, 13

    @item_with_tiers.quantity = 21
    assert_equal @item_with_tiers.discount, 33
  end
end
