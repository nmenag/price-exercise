# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/base'
require_relative '../lib/optional'

class OptionalTest < Test::Unit::TestCase
  def setup
    @optional_one = Optional.new('test_optional', price_model: 'fixed', price: 12)
    @optional_two = Optional.new('test optional two', price_model: 'user_defined')
  end

  test 'initialize' do
    assert_equal @optional_one.name, 'test_optional'
    assert_equal @optional_one.code, 'test_optional'
    assert_equal @optional_one.price_model, 'fixed'
    assert_equal @optional_one.price, 12
    assert_equal @optional_one.min, 0.1

    assert_equal @optional_two.name, 'test optional two'
    assert_equal @optional_two.code, 'test_optional_two'
    assert_equal @optional_two.price_model, 'user_defined'
    assert_equal @optional_two.price, 0.0
    assert_equal @optional_two.min, 0.1
  end

  test '.find' do
    optional = Optional.find('bag')

    assert_equal optional.class, Optional
    assert_equal optional.code, 'bag'
    assert_equal optional.price, 2
  end
end
