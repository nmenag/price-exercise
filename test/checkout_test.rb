# frozen_string_literal: true

require 'test/unit'
require 'byebug'

require_relative '../lib/base'
require_relative '../lib/product'
require_relative '../lib/optional'
require_relative '../lib/promotion'
require_relative '../lib/checkout'

class CheckoutTest < Test::Unit::TestCase
  def setup
    @checkout = Checkout.new

    @product = Product.new('orange', price_model: 'fixed', price: 12)
    @tiers = [{ from: 1, to: 10, price: 10 },
              { from: 11, to: 20, price: 9 },
              { from: 21, to: 30, price: 8 }]

    @product_with_tiers = Product.new('apple', price_model: 'tiered',
                                               tiers: @tiers)

    @promotion_unit = Promotion.new('test promotion', discount: 0.2,
                                                      requirements: { min_units: 20 })

    @promotion_amount = Promotion.new('test promotion', discount: 0.1,
                                                        requirements: { min_amount: 100 })
  end

  test 'initialize' do
    assert_equal @checkout.items.class, Array
  end

  test '#add when the product does not exist' do
    assert_equal @checkout.add_product('product_no_exist', 15), false
  end

  test '#add without optionals' do
    checkout = Checkout.new
    checkout.add_product('orange', 10)
    checkout.add_product('apple', 15)
    assert_equal checkout.total, 244 # bag added
  end

  test '#add with optionals by 10' do
    checkout = Checkout.new
    checkout.add_product('orange', 10)
    checkout.add_product('apple', 15)
    checkout.add_tip
    assert_equal 268, checkout.total
  end

  test '#add with tip less that 10 percentage' do
    checkout = Checkout.new
    checkout.add_product('orange', 10)
    checkout.add_product('apple', 15)
    checkout.add_tip(0.09)
    assert_equal 268, checkout.total
  end

  test '#add with custom tip' do
    checkout = Checkout.new
    checkout.add_product('orange', 10)
    checkout.add_product('apple', 15)
    checkout.add_tip(0.15)
    assert_equal 280, checkout.total
  end
end
