# frozen_string_literal: true

require_relative 'product'

class Item < Base
  attr_reader :code
  attr_accessor :quantity, :discount

  def initialize(code, quantity = 1)
    @code = code
    @quantity = quantity.to_i.nonzero? || 1
  end

  def value
    product.tiers_value_or_price(quantity)
  end

  def product
    Product.find(code)
  end

  def discount
    (value * promotion&.discount.to_f).to_i
  end

  def promotion
    promotions = []

    Promotion.all.each do |promotion|
      if promotion.min_units? && quantity > promotion.min_units
        promotions << promotion
      elsif promotion.min_amount? && value > promotion.min_amount
        promotions << promotion
      end
    end

    promotions.max_by(&:discount)
  end
end
