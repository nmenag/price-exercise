# frozen_string_literal: true

require 'yaml'

class Base
  def initialize(*_args); end

  def attrs
    instance_variables.map { |ivar| instance_variable_get ivar }
  end

  def self.data
    YAML.load_file('data/data.yml')
  end
end
