# frozen_string_literal: true

require 'byebug'
require_relative 'item'
require_relative 'optional'
require_relative 'promotion'

class Checkout < Base
  attr_accessor :total, :items, :tip_value

  def initialize
    @items = []
  end

  def add_product(code, quantity)
    product = find_product(code)
    return false if product.nil?

    item = Item.new(product.code, quantity)
    @items.push(item)
  end

  def add_tip(percentage = 0.1)
    optional = Optional.find('tip')
    percentage = percentage > optional.min ? percentage : optional.min
    self.tip_value = (total * percentage).to_i
  end

  def total
    value = items.inject(0.0) { |v, i| v + i.value }
    value += apply_optionals # apply the bag and the tip
    value -= apply_discount
    value
  end

  private

  def apply_optionals
    return 0.0 if items.empty? # Does not apply the bag value when the cart is empty

    bag_value + tip_value.to_i
  end

  def bag_value
    optional = Optional.find('bag')
    optional&.price.to_i
  end

  def apply_discount
    items.max_by(&:discount)&.discount || 0.0
  end

  def find_product(code)
    Product.find(code)
  end
end
