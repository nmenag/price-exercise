# frozen_string_literal: true

class Optional < Base
  attr_accessor :code, :name, :price_model, :price, :min

  def initialize(code, attrs)
    attrs = attrs.transform_keys(&:to_sym)
    @price_model = attrs[:price_model]
    @price = attrs[:price] || 0.0
    @min = attrs[:min] || 0.1
    @code = code.downcase.tr(' ', '_')
    @name = code
  end

  def self.find(code)
    records = data['optionals']
    attrs = records[code.to_s].transform_keys(&:to_sym)
    new(code, attrs) unless attrs.nil?
  end

  def self.all
    records = data['optionals']
    records.map { |k, v| new(k, v) }
  end
end
