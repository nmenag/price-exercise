# frozen_string_literal: true

class Promotion < Base
  attr_accessor :code, :name, :discount, :requirements

  def initialize(code, attrs)
    attrs = attrs.transform_keys(&:to_sym)
    @code = code.downcase.tr(' ', '_')
    @name = code
    @discount = attrs[:discount]
    @requirements = attrs[:requirements].transform_keys(&:to_sym)
  end

  def min_units
    requirements[:min_units].to_f
  end

  def min_units?
    min_units.positive?
  end

  def min_amount
    requirements[:min_amount].to_f
  end

  def min_amount?
    min_amount.positive?
  end

  def self.find(code)
    records = data['promotions']
    attrs = records[code.to_s].transform_keys(&:to_sym)
    new(code, attrs) unless attrs.nil?
  end

  def self.all
    records = data['promotions']
    records.map { |k, v| new(k, v) }
  end
end
