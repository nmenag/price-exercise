# frozen_string_literal: true

class Store
  def initialize; end

  def products
    rows = list(Product.all)
    title = 'List Products'
    headers = ['price Model', 'Price', 'Name', 'Code', 'tiers']
    products_printable(title, headers, rows)
  end

  def find_product(code)
    product = Product.find(code)
    return if product.nil?

    title = 'Products list'
    headers = ['price Model', 'Price', 'Name', 'Code', 'tiers']
    rows = list([product])
    products_printable(title, headers, rows)
  end

  def promotions
    rows = list(Promotion.all)
    title = 'Promotions list'
    headers = %w[Name code discount requirements]
    products_printable(title, headers, rows)
  end

  private

  def products_printable(title, headers, rows)
    Terminal::Table.new(title: title, headings: headers, rows: rows)
  end

  def list(records)
    array = []

    records.each do |record|
      array.push(record.attrs)
    end

    array
  end
end
