# frozen_string_literal: true

class Product < Base
  attr_accessor :code, :name, :price_model, :price, :tiers

  def initialize(code, attrs)
    attrs = attrs.transform_keys(&:to_sym)

    @price_model = attrs[:price_model]
    @price = attrs[:price] || 0.0
    @code = code.downcase.tr(' ', '_')
    @name = code
    @tiers = attrs[:tiers]&.map { |t| t.transform_keys(&:to_sym) } || {}
  end

  def self.find(code)
    records = data['products']
    attrs = records[code]&.transform_keys(&:to_sym)
    Product.new(code, attrs) unless attrs.nil?
  end

  def self.all
    records = data['products']
    records.map { |k, v| new(k, v) }
  end

  def tiers_value_or_price(quantity = 1)
    value =
      if non_tier_apply?
        price
      else
        tiers_range.select { |qty| qty === quantity }&.values&.first || price
      end

    value * quantity
  end

  def fixed?
    price_model == 'fixed'
  end

  def tiered?
    price_model == 'tiered'
  end

  def non_tier_apply?
    fixed? || tiers.nil?
  end

  private

  def tiers_range
    # structure {1..10=>10, 11..20=>9, 21..30=>8}
    tiers.each_with_object({}) { |(k, _v), h| h[k[:from]..k[:to]] = k[:price]; }
  end
end
